// Fill out your copyright notice in the Description page of Project Settings.


#include "ReaperCharacter.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PaperFlipbookComponent.h"

AReaperCharacter::AReaperCharacter()
{
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->SetRelativeRotation(FRotator(-30.0f, -90.0f, 0.0f));
	SpringArmComponent->TargetArmLength = 500.0f;
	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritYaw = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bEnableCameraLag = true;
	SpringArmComponent->bEnableCameraRotationLag = true;
	SpringArmComponent->CameraLagSpeed = 4.0f;
	SpringArmComponent->CameraRotationLagSpeed = 4.0f;
	SpringArmComponent->CameraLagMaxDistance = 600.0f;


	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	bIsMoving = false;
}

void AReaperCharacter::BeginPlay()
{

	Super::BeginPlay();

	OnCharacterMovementUpdated.AddDynamic(this, &AReaperCharacter::Animate);
}

void AReaperCharacter::AddMovementInput(FVector WorldDirection, float ScaleValue, bool bForce)
{
	auto MovementComponent = GetMovementComponent();

	if (MovementComponent)
	{
		MovementComponent->AddInputVector(WorldDirection * ScaleValue, bForce);
	}
	else
	{
		Internal_AddMovementInput(WorldDirection * ScaleValue, bForce);
	}
}

void AReaperCharacter::SetCurrentAnimationDirection(FVector const& Velocity)
{
	const float x = Velocity.GetSafeNormal().X;
	const float y = Velocity.GetSafeNormal().Y;

	bIsMoving = x != 0.0f || y != 0.0f;

	if (bIsMoving)
	{
		if (y > 0.0f && abs(x) < 0.5f)
		{
			CurrentAnimationDirection = EAnimationDirection::Down;
		}
		else if (y > 0.5f && x > 0.5f)
		{
			CurrentAnimationDirection = EAnimationDirection::DownRight;
		}
		else if (y > 0.5f && x < -0.5f)
		{
			CurrentAnimationDirection = EAnimationDirection::DownLeft;
		}
		else if (y < 0.5f && abs(x) < 0.5f)
		{
			CurrentAnimationDirection = EAnimationDirection::Up;
		}
		else if (y < -0.5f && x > 0.5f)
		{
			CurrentAnimationDirection = EAnimationDirection::UpperRight;
		}
		else if (y < -0.5f && x < -0.5f)
		{
			CurrentAnimationDirection = EAnimationDirection::UpperLeft;
		}
		else if (abs(y) < 0.5f && x > 0.0f)
		{
			CurrentAnimationDirection = EAnimationDirection::Right;
		}
		else
		{
			CurrentAnimationDirection = EAnimationDirection::Left;
		}
	}
}

void AReaperCharacter::Animate(float DeltaTime, FVector OldLocation, FVector const OldVelocity)
{
	SetCurrentAnimationDirection(OldVelocity);

	if (OldVelocity.Size() > 0.0f)
	{
		switch (CurrentAnimationDirection)
		{
		case EAnimationDirection::Up:
			GetSprite()->SetFlipbook(Flipbooks.WalkUp);
			break;
		case EAnimationDirection::Down:
			GetSprite()->SetFlipbook(Flipbooks.WalkDown);
			break;
		case EAnimationDirection::Left:
			GetSprite()->SetFlipbook(Flipbooks.WalkLeft);
			break;
		case EAnimationDirection::Right:
			GetSprite()->SetFlipbook(Flipbooks.WalkRight);
			break;
		case EAnimationDirection::UpperLeft:
			GetSprite()->SetFlipbook(Flipbooks.WalkUpperLeft);
			break;
		case EAnimationDirection::UpperRight:
			GetSprite()->SetFlipbook(Flipbooks.WalkUpperRight);
			break;
		case EAnimationDirection::DownLeft:
			GetSprite()->SetFlipbook(Flipbooks.WalkDownLeft);
			break;
		case EAnimationDirection::DownRight:
			GetSprite()->SetFlipbook(Flipbooks.WalkDownRight);
		default: 
			break;
		}
	}
	else
	{
		switch (CurrentAnimationDirection)
		{
		case EAnimationDirection::Up:
			GetSprite()->SetFlipbook(Flipbooks.IdleUp);
			break;
		case EAnimationDirection::Down:
			GetSprite()->SetFlipbook(Flipbooks.IdleDown);
			break;
		case EAnimationDirection::Left:
			GetSprite()->SetFlipbook(Flipbooks.IdleLeft);
			break;
		case EAnimationDirection::Right:
			GetSprite()->SetFlipbook(Flipbooks.IdleRight);
			break;
		case EAnimationDirection::UpperLeft:
			GetSprite()->SetFlipbook(Flipbooks.IdleUpperLeft);
			break;
		case EAnimationDirection::UpperRight:
			GetSprite()->SetFlipbook(Flipbooks.IdleUpperRight);
			break;
		case EAnimationDirection::DownLeft:
			GetSprite()->SetFlipbook(Flipbooks.IdleDownLeft);
			break;
		case EAnimationDirection::DownRight:
			GetSprite()->SetFlipbook(Flipbooks.IdleDownRight);
		default:
			break;
		}
	}
}
