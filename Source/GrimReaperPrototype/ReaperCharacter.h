// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "PaperFlipbook.h"
#include "ReaperCharacter.generated.h"

UENUM(BlueprintType, Category = "Animation")
enum class EAnimationDirection : uint8
{
	Up,
	Down,
	Left,
	Right,
	UpperLeft,
	UpperRight,
	DownLeft,
	DownRight,
};

USTRUCT(BlueprintType, Category = "Animation")
struct FAnimationFlipbooks
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* IdleUp {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* IdleDown {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* IdleLeft {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* IdleRight {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* IdleUpperLeft {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* IdleUpperRight {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* IdleDownLeft {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* IdleDownRight {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* WalkUp {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* WalkDown {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* WalkLeft {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* WalkRight {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* WalkUpperLeft {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* WalkUpperRight {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* WalkDownLeft {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UPaperFlipbook* WalkDownRight {nullptr};
};

/**
 * A paper character using 2D sprites
 */

UCLASS()
class GRIMREAPERPROTOTYPE_API AReaperCharacter : public APaperCharacter
{
	GENERATED_BODY()
	
	public:

		AReaperCharacter();

	protected:

		virtual void BeginPlay() override;

		virtual void AddMovementInput(FVector WorldDirection, float ScaleValue, bool bForce) override;

		UFUNCTION(BlueprintCallable, Category = "ReaperCharacter|Animation")
		void SetCurrentAnimationDirection(FVector const& Velocity);

		UFUNCTION(BlueprintCallable, Category = "ReaperCharacter|Animation")
		void Animate(float DeltaTime, FVector OldLocation, FVector const OldVelocity);

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class USpringArmComponent* SpringArmComponent;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UCameraComponent* CameraComponent;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ReaperCharacter|Config")
		EAnimationDirection CurrentAnimationDirection {EAnimationDirection::Down};

		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ReaperCharacter|Config")
		FAnimationFlipbooks Flipbooks;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ReaperCharacter|Config")
		uint8 bIsMoving : 1;
};
